# CPS 474 Final Project: Secure Login System

## Team members
1. Andre Cullen: cullena1@udayton.edu
2. Anthony Avila: avilaa1@udayton.edu

### Project Summary

Our plan for this project was to continue using the Secure login system that we created in Lab 4 & 5 of CPA 474 and to continue to research everything that goes into developing a secure login system. We will look into how to implement a strong password policy for when a user is registering into the system, security against SQL Injection, XSS Scripting, CSRF attacks

Strong Password Policy has been implemented so for the user to create an account, the password has to be at least 8 characters long and must contain at least 1 symbol, number and uppercase letter.

We are still working on implementing the SQL Injection protection and also creating a new php page so the user that does not yet have an account can create an account to register into our website.

We have implemented the SQL Injection protection by using prepared statements and adding a new securelogin function to check instead of the checklogin or mockchecklogin that we were previously using in class CPS 474.

All Passwords are hashed in the SQL database using Hashing functions and Salt, and we also created a new SQL database for this Final Project to distinguish between the database we used in class for Lab 4 & 5.

We are currently fixing bugs on the implementation for the reCaptcha authentication feature which we are also implementing into our login page.

Our plan for future sprints is to implement security protections against XSS attacks, CSRF attacks and Command Injection attacks.

In terms of SQL, we want to create a Role Based Access Control protocol and separate all of our users with roles as an admin or a regular user. By doing this we will not allow any normal user to input any commands or anything that he/she is not supposed to in our login page.

We plan to protect against XSS by sanitizing all html inputs and outputs both on the Server Side and the Client Side. We plan to do this by Removing/Encoding special characters in inputs and outputs and also preventing some characters to be injected into our system like for example, "< script >"!

We plan to protect against CSRF attacks by implementing a Secret Validation Token where if the attacker does not have that Secret Token then the website will not accept any of his/her incoming requests.

We plan to implement SSL Keys and Certificates to have our website as https.

In terms of research, our login would be much more secure if in the future we would have a firewall implemented into our system.

We want to use the verify.php to verify both the captcha authentication and also to re verify the email and password to match the ones in our database. This will be for login, not for the register page. Register page will only be for new users. 

## Introduction
The motivation for this project was for us to see what it actually took and everything that actually went into building a completely secure login system. We were motivated to do this work as we became more intrested on other ways that we can secure the system as just lab 5 made us wonder how other companies do it and what do they have to do to make sure their login system is secure for their users. we got a small taste in that in lab 5 the small changes we did made a huge differenece in the way the program functioned. since this class started we as a group saw security measures we have never heard of and the small easy ways that we can get  hacked from were shocking to us. we found a ton of security measures that we can implement as there was hundreds of ways someone could have gotten access to a website. we had to narrow down the ones we thought went with the course of the class and were more important to get a better understanding of the class. we had a solid foundation from class but still we needed to do more research inorder to figure out what we truly needed to implement the features that we wanted. 

## Background: 
For decades we have seen how hackers are able to hack into big companies and steal a lot of vital and personal information from the users from that system. Through the years, hackers get better and better finding new techniques and strategies to compromise and exploit different systems. One of the most important things a website holds in terms of data is the personal information of all of their users. These companies need to secure their systems to the level where they will make it harder and harder every day for new hackers to exploit and steal information from their system. OWASP TOP 10 places SQL Injection, XSS Attacks, CSRF attacks all as extremely dangerous attacks that companies should take the upper hand and try to prevent before becoming the victims of one of these hacks. We saw during our time in class the login system presented to us in lab 5 from this we started thinking what other methods we can implement in that in order to make that system more secure. we learned so much from the labs after that and other new security features presneted to us that we thought those were a no brainerr that they had to be implemented in our project. we also knew from our personal experience and other websites we have incountered that having a recapta feature would stop a lot of the problems of the website knowing when a human or a robot was trying to get access to the website. we need saw a need to get more restrictions so that anyone cant get into the site. thats where we got the idea of running a more role based access control to the login system. we did a good amount of research to understand what and how to implement. now with technology geetting more stronger and being able to attack and manipulate more secuirty features we saw this as a great oppurinty to try and see if we can prevent some of these things from happening. 

## Project Description: 
For this Project, we decided to research all that actually goes into implementing a Secure Login System but we also wanted to expand upon the implementation from Lab 5 and continue adding some more security features. We will try and implement SQL Injection Protection, Secure Password Policy for all users, Role Based Access Control to differentiate an admin user from a normal user, and also implement reCaptcha Security onto our Secure Login System. 

To solve these attacks there are several ways we were able to identify. 

### Research: 

#### SQL Injection Prevention
SQL Injection attacks is when an attacker/hacker gains information of a particular system by exploiting any type of user input into the website by injecting a malicious SQL Injection that depending on the hacker and their intentions could potentially gain username and password information of users stored in the database and then potentially gain root access into the system. SQLi attacks essentially happen because the developer is not vaildating data being inputted into the system before passing them to SQL queries. 

To prevent SQL Injection Attacks, it is important for us as developers to use prepared statements on all potential inputs of the system to prevent from an attacker from implementing a SQL command and accesing our entire system, database, source code, etc. 

#### XSS Attack Prevention
Cross Site Scripting (XSS) attacks and one of the reasons that they are so dangerous for all web applications is because it can come in a variety of forms. There are basically three types of XSS attacks, two were originally discovered, Stored and Reflected, but recently there was a third type of XSS attack discovered, that being DOM Based XSS Attacks. Stored XSS attacks usually happen when developers have user inputs being stored for example inside their database or any other place where the user is able to store data inside the web app. Reflected XSS attacks in the other hand occur when inputs are being returned in a search result, message or any other thing that requires an input from the user. 

In recent years, to make things a little bit more clear, the research community for security vulnerabilities in web applications started using two terms to describe all 3 of the pre established XSS types of attacks in web applications. These two new categories are Server XSS and Client XSS. Client XSS is when unvalidated user inputs is attempting to update the DOM using malicious js code. Client XSS are preventted by using safe api's from JavaScript. Server XSS in the other hand are when unvalidated user inputs are being generated as an HTTP request. Server XSS attacks can be prevented by encoding any and all inputs and outputs coming in and out of our system. 

In terms of XSS Attacks, it is vital to sanitize and validate all html inputs and outputs by encoding them using either htmlentities or html special chars. htmlspecialchars(input) is used when a developer wants to encode any type of input before the input can actually be executed. htmlentities in the other hand is used when the developer wants to convert the input before displaying it on the webpage. 

#### CSRF Attack Prevention
Cross Site Request Forgery attacks originate because a server cannot differentiate between a valid request and an invalid request. In addition, the server also does not re-authenticate new requests. These are often the security vulnerabilties that a lot of security developers forget to implement in a lot of their systems, although it is not such a hard vulnerability to fix. 

For Cross Site Request Forgiery attacks, to prevent them from happening we can use a randomly generating Secret Validation Token that is constantly regenerating to maintain it more secure. True and valid requests will contain this secret token but a request that is forged by a hacker will not contain any token or a false and wrong token that will not match with our csrf secret token, at that point a CSRF attack has been identified and will be prevented. 

#### Isolate Database file from the server
It is important to isolate and possibly remove the database file from the server so that it is not able to be accessed through an attack. If our database file is compromised, there goes a "map" to how they can attack our website. To prevent this, we need to remove the database file of our project from the server so that it cannot be accessed remotely and read by an attacker. 

#### ReCaptcha Security
ReCaptcha is a security mechanism placed to differentiate human users from bot users. The implementation takes various forms: Checkbox, Identifying Pictures or correctly spelling an array of random letters and numbers. We can implement this either manually or by using googles reCaptcha authentication code to put into our website with a public key generated from google for our system. 

#### Run System on HTTPS
To make sure all of the data going through our system is being encrypted properly, we must make the login system run on https. We are able to do this by generating SSL Certificates and SSL Keys for our login system in particular. By doing this, our website could be run on https and by doing so all of the data going through our website will be encrypted. It is important to note that adding https to our website does not prevent any of the attacks mentioned ealier. That being said, it is still very important to make sure that all of the HTTP data that is traveling through our system will be encrypted.

#### Role Based Access Control
Another popular security implementation is the ability to control what users can see and what some users cannot see in our system. To do this, we need to differentiate from a normal user to an admin user. We can do this by making all of the users in our system have a username, passoword and adminuser value. Our users will be able to set their own username and password, but they will not be able to setup their adminuser value. All users will default to an adminuser value of 0, making them normal users. The users that we, the founders, decide that can view specific content of our website, we will make their adminuser value as 1. By doing this, these users will have admin access to our website and have unique features and access to some pages that a normal user might not have access. 

#### Hashing Passwords in DB
Our database, ss_team9, stores all of the usernames and passwords of users in our website. If we were to just leave it like this, you could see all of the passwords of all users in our website directly from our database. To prevent this we have to hash our passwords using salt, and the passwords will be shown as cyphertext in the database but will hold the original text of the password. The information will just be encrypted in cypher text. 

#### Firewall 
A Firewall exists to prevent unauthorized access into or out of a network. If a company really wanted to make sure that they were extra secure and implement different layers of security into their system, they would most likely want to invest in a proper firewall for their network. 

#### Using Strong Password
A very important security issue in our society today is the use of very simple passwords among all our communitites due to the fact that they are easy to remember, but they are also very easy to hack if your password has any words or personal information like your pets name. Therefore, to actually make sure that everyone is having strong passwords we need to either make them randomly generated, but then they will be extremely hard to remember, or to require strong policies to prevent users from having weak and easy hackable passwords. 
For our implementation, we are simply creating a simple Strong Password Policy. This strong password policy will require the user to input a password that has at least 8 characters, 1 UpperCase Letter, 1 symbol and 1 number. 

NIST is the National Institution for Standards in Technology's Digital Identity Guidelines and they specify some of the requirements users should have when creating strong passwords. Some of their recommendations include: Don't require period password resets, this is due to the fact that when users regularly have to change their passwords then they don't really make any significant changes and their passwords if have been identified in the past then will still be easy to identify with these small changes. Another recommendation is to focus on password length instead of complexity. Finally, the also recommend to ensure that users aren't using any commonly used words for their passwords, it should be a well blended combination of strength, complexity and length. 

An additional security measure that is very important to make when speaking of login systems is two factor authentication. This ensures that when a user logs into a website, they have to re authenticate through a call, text, or push of a requested button to make sure that there are two parameters of security when logging into a system. 


### Implementation: 
We started by creating a new database by the name of ss_team9, the reason for this is because we wanted to have a separate database from lab5 where we could store just our information for this project. To be able to access just the database we just created, we had to create new users with admin access to the database. By doing this, now we were able to access just our own database by connecting to mysql with the user we created. 

##### Screen Shot of Database accessed through User and code of Database.sql and Database.php: 
![Database](https://i.ibb.co/WvFvhSf/2021-12-06-10.png)
![Database php](https://i.ibb.co/DGL3dK5/2021-12-06-11.png)

After creating the database, we implemented a Strong Password Policy for all our users to follow when creating an account in our system. All passwords must have at least 8 characters, 1 Capital Letter, 1 number and 1 symbol. If the user inputs a password that does not match with out established policy, the user will get a notification on the spot to change their password so it matches our policy. 

##### Screen Shot of Code for Strong Password: 
![StrongPass](https://i.ibb.co/SnW41HV/2021-12-06-8.png)

Once we made sure our Strong Password Policy was working properly, we tried to prevent SQL Injection attacks on all inputs of our website. All inputs of our website would be the login page username and password, and the registration page username and password. We prevented against SQL Injection attacks by using Prepared Statements on all inputs of our system.

##### Screen Shot of Code for SQL Injection Prevention: 

![SQLiPrev](https://i.ibb.co/JppgW2Y/2021-12-06-9.png)

To implement reCaptcha Security, we had 2 ways of implementing it. We tried implementing reCaptcha manually by creating a design that mimicks the design of a reCaptcha and displays a random array of 6 letters. The other option we tried was to implement reCaptcha with the google api and to do this we had to register with google and get a public key to make the feature work properly. By implementing this, we are now making sure that all users that enter our system are human and not bot users. 

##### Screen Shot of reCaptcha Security: 

![reCaptcha](://i.ibb.co/xFjkRZp/2021-12-06-7.png)


Finally, we implemented Role Based Access Control on our system to differentiate from admin users and normal users. We do this by making the user have 3 values in our database. All users that enter our system have to setup a username and password, but all users also have an admin user value that defaults all users to a value of 0 meaning that they are regular users. Admin users in the other hand have a adminuser value of 1 and this value allows them to access some features of the system that regular users are not able to access. 

##### Screen Shot of Role Based Access Control Code on database.sql or database.php: 

![RBAC](https://i.ibb.co/pLkcSNV/2021-12-06-6.png)




# Results
(Expected length: 1-2 pages)
Explain what we were able to accomplish with the implementation of this project: 

### Screenshots of Secure Login System: 
Login Page with ReCaptcha
![Login](https://i.ibb.co/Y8XNDHm/Screen-Shot-2021-12-15-at-8-08-29-PM.png)

Registration Page with Strong Password Policy
![Register](https://i.ibb.co/pdC4zZf/Screen-Shot-2021-12-15-at-8-07-40-PM.png)

Database Design: 
Show how Users Table is set up in Database 
![UsersDB](https://i.ibb.co/4PWYWKc/Screen-Shot-2021-12-15-at-8-07-18-PM.png)

### Contribution 
Andre
1. Created new DB on MySQL also using database.sql
2. Created a new registration page for new users. 
3. Implemented Strong Password Policy for new users creating an account. 
4. Implemented SQL Injection protection by using Prepared statements on all inputs of the login page. 
5. Documentation on README
6. Implemented Role Based Access Control
7. Continued developing reCaptcha security
8. Implemented Sessions into website
9. Fixed DB connection issue 

Anthony
1. Created new documents for reCaptcha security feature
2. Implementing reCaptcha feature to work on login page
3. Documentation on README
4. More Research on SQL Security Features we could implement
5. Started creating Final Project Report

# Project Prototype

## Last Bit Bucket Commit: 
Andre: https://bitbucket.org/ss-lbs-f21-team9/ss-lbs-team9/commits/bc4f1c19b90bf407351581a94e4a24ea53700144

Anthony: https://bitbucket.org/ss-lbs-f21-team9/ss-lbs-team9/commits/d1786c9a812b51502623bad9f66758b817de1702

## README File

# README 
# CPS 474 - Software Security

Source: https://bitbucket.org/ss-lbs-f21-team9/ss-lbs-team9/src/master/
Trello: https://trello.com/b/rFNWKSLP/cps-474-final-project-secure-login

University of Dayton

Department of Computer Science

CPS 474 - Software Security, Fall Semester 2021

Instructor: Dr. Phu Phung


## Software Security Project 

### Secure Login (SQL & PHP)
This is our Team 9, Anthony Avila & Andre Cullen, repository for our Final Project for CPS 474 Software Security. 

# Project Overview: Secure Login System following Lab 5
Our plan for this project was to continue using the Secure login system that we created in Lab 4 & 5 of CPA 474 and we wanted to update and continue making it more secure by adding more security features from SQL & PHP and some database updates. 
We are starting to work through the first sprints of this project and want to begin by making a Strong Password Policy for our Login.

Strong Password Policy has been implemented so for the user to create an account, the password has to be at least 8 characters long and must contain at least 1 symbol, number and uppercase letter. 

We are still working on implementing the SQL Injection protection and also creating a new php page so the user that does not yet have an account can create an account to register into our website. 

We have implemented the SQL Injection protection by using prepared statements and adding a new securelogin function to check instead of the checklogin or mockchecklogin that we were previously using in class CPS 474. 

All Passwords are hashed in the SQL database using Hashing functions and Salt, and we also created a new SQL database for this Final Project to distinguish between the database we used in class for Lab 4 & 5. 

We are currently fixing bugs on the implementation for the reCaptcha authentication feature which we are also implementing into our login page. 

Our plan for future sprints is to implement security protections against XSS attacks, CSRF attacks and Command Injection attacks. 

In terms of SQL, we want to create a Role Based Access Control protocol and separate all of our users with roles as an admin or a regular user. By doing this we will not allow any normal user to input any commands or anything that he/she is not supposed to in our login page.

We plan to protect against XSS by sanitizing all html inputs and outputs both on the Server Side and the Client Side. We plan to do this by Removing/Encoding special characters in inputs and outputs and also preventing some characters to be injected into our system like for example, "< script >"! 

We plan to protect against CSRF attacks by implementing a Secret Validation Token where if the attacker does not have that Secret Token then the website will not accept any of his/her incoming requests. 

We plan to implement SSL Keys and Certificates to have our website as https. 

In terms of research, our login would be much more secure if in the future we would have a firewall implemented into our system. 

We want to use the verify.php to verify both the captcha authentication and also to re verify the email and password to match the ones in our database. This will be for login, not for the register page. Register page will only be for new users.  


# Team members
1. Anthony Avila
2. Andre Cullen 

## Project Objectives and Plan 
1. Create a Strong Password Policy through SQL
2. Hash Passwords in Database
3. Implement SQL Injection Protection
4. Implement reCaptcha authentication method
5. Implement Role Based Access Control
7. Implement Command Injection attack protection

## Security Implementations
1. Secure Password Policy
2. Implementing Captcha Security

## PHP Security Implementations
1. SQL Injection Prevention
We are using Prepared Statements on all html inputs of the secure login system to make sure that our system is completely secure against SQL Injection Attacks. 
2. Command Injection Prevention

## Sprint 0: Researching new SQL Security Features to Implement
1. Researched new SQL Security Features
2. Copy Lab 5 code content to our current repository
3. Modify current code to make it our own for this project
4. Implement 2 new SQL security features for Login

# Done: 
1. Researched new SQL Security Features
2. Copy Lab 5 code to our current repository
3. Implement Strong Password Policy 

## Sprint 1: Creating new DB and implementing Strong Password without bugs
1. Created new DB 
2. Implemented Strong password correctly
3. Created new Registration Page

### Sprint 1 accomplishments: 
1. Created new DB by the name of ss_team9
We created new users and granted full access to the database, therefore we had a way to connect solely into the database of our project instead of seeing all of the databases in the entire system. 
2. Implemented Strong Password Policy
Every new user that registers into our secure login system, will have to create a password that matches with our policy: at least 1 CAPITAL letter, 1 symbol, 1 number and at least 8 characters. 
3. Created new Registration page for our website so that new users could also register into the site. 

## Sprint 2: Implementing SQL Injection Protection
1. Implemented SQL Injection Protection with Prepared Statements
2. Added newUser so that after a user registers, the username and password are directly stored in the DB. 

### Sprint 2 Accomplishments: 
1. Implemented SQL Injection Protection
We implemented Prepared Statements on all user inputs available in our login system to make sure that we are preventing SQL Injections at all layers of the system. 

## Sprint 3: Implementing reCaptcha security feature
1. Researched how to implement reCaptcha
2. Started implementing reCaptcha

### Sprint 3 Accomplishments: 
1. Figured out a manual way to implement reCaptcha
We will try and implement reCaptcha manually to see if we are able to get it fully working. 
2. Figured out a way to implement reCaptcha with google api. 

## Sprint 4: Researching more SQL Security Features for our Login. 
1. RBAC 

### Sprint 4 Accomplishments: 
1. Implemented Role Based Access Control
We are differentiating between regular users and Admin Users into our website. Admin users, will be able to see all users with accounts in our website. Regular users will just be able to register into the website and login with their user information. We implemented this by adding an attribute to the Users table in our database, therefore our database now stores a users: username, password, and adminuser value. Regular users will default to a adminuser value of 0, and adminusers will have a adminuser value of 1. 

## Sprint 5: 
1. Fix DB connection issue
2. Fix reCaptcha issue

### Sprint 5 Accomplishments: 
1. Fixed DB connection issue
Fixed DB connection issue by creating a new user with access granted to the entire DB for our project. This way we could access the DB directly through that user. To fix the issue, I also had to change the credentials for the DB in the database.php file. 
2. Made progress with reCaptcha issue
reCaptcha is displaying the image properly in our login system where the text should be, but the random letters aren't displaying properly. We will try to fix this issue before our final presentation but if not, we will try to implement googles reCaptcha method. 

## Sprint 6: 
1. Fix registration issue
2. Add a page where only admin user can view all the users of the website. 
3. Fix reCaptcha issue or implement google reCaptcha


### Contribution guidelines ###
Andre
1. Created new DB on MySQL also using database.sql
2. Created a new registration page for new users. 
3. Implemented Strong Password Policy for new users creating an account. 
4. Implemented SQL Injection protection by using Prepared statements on all inputs of the login page. 
5. Documentation on README
6. Implemented Role Based Access Control
7. Continued developing reCaptcha security
8. Implemented Sessions into website
9. Fixed DB connection issue 

Anthony
1. Created new documents for reCaptcha security feature
2. Implementing reCaptcha feature to work on login page
3. Documentation on README
4. More Research on SQL Security Features we could implement
5. Started creating Final Project Report 

### Appendix
Unlimited appendix pages to show e.g., screenshots of your demo.


![appendix](https://i.ibb.co/5rnNKMX/Screen-Shot-2021-12-06-at-10-17-51-PM.png)
