# README #
# CPS 474 - Software Security

Source: https://bitbucket.org/ss-lbs-f21-team9/ss-lbs-team9/src/master/
Trello: https://trello.com/b/rFNWKSLP/cps-474-final-project-secure-login

University of Dayton

Department of Computer Science

CPS 474 - Software Security, Fall Semester 2021

Instructor: Dr. Phu Phung


## Software Security Project 

### Secure Login (SQL & PHP)
This is our Team 9, Anthony Avila & Andre Cullen, repository for our Final Project for CPS 474 Software Security. 

# Project Overview: Secure Login System following Lab 5
Our plan for this project was to continue using the Secure login system that we created in Lab 4 & 5 of CPA 474 and we wanted to update and continue making it more secure by adding more security features from SQL & PHP and some database updates. 
We are starting to work through the first sprints of this project and want to begin by making a Strong Password Policy for our Login.

Strong Password Policy has been implemented so for the user to create an account, the password has to be at least 8 characters long and must contain at least 1 symbol, number and uppercase letter. 

We are still working on implementing the SQL Injection protection and also creating a new php page so the user that does not yet have an account can create an account to register into our website. 

We have implemented the SQL Injection protection by using prepared statements and adding a new securelogin function to check instead of the checklogin or mockchecklogin that we were previously using in class CPS 474. 

All Passwords are hashed in the SQL database using Hashing functions and Salt, and we also created a new SQL database for this Final Project to distinguish between the database we used in class for Lab 4 & 5. 

We are currently fixing bugs on the implementation for the reCaptcha authentication feature which we are also implementing into our login page. 

Our plan for future sprints is to implement security protections against XSS attacks, CSRF attacks and Command Injection attacks. 

In terms of SQL, we want to create a Role Based Access Control protocol and separate all of our users with roles as an admin or a regular user. By doing this we will not allow any normal user to input any commands or anything that he/she is not supposed to in our login page.

We plan to protect against XSS by sanitizing all html inputs and outputs both on the Server Side and the Client Side. We plan to do this by Removing/Encoding special characters in inputs and outputs and also preventing some characters to be injected into our system like for example, "< script >"! 

We plan to protect against CSRF attacks by implementing a Secret Validation Token where if the attacker does not have that Secret Token then the website will not accept any of his/her incoming requests. 

We plan to implement SSL Keys and Certificates to have our website as https. 

In terms of research, our login would be much more secure if in the future we would have a firewall implemented into our system. 

We want to use the verify.php to verify both the captcha authentication and also to re verify the email and password to match the ones in our database. This will be for login, not for the register page. Register page will only be for new users.  


# Team members
1. Anthony Avila
2. Andre Cullen 

## Project Objectives and Plan 
1. Create a Strong Password Policy through SQL
2. Hash Passwords in Database
3. Implement SQL Injection Protection
4. Implement reCaptcha authentication method
5. Implement Role Based Access Control
7. Implement Command Injection attack protection

## Security Implementations
1. Secure Password Policy
2. Implementing Captcha Security

## PHP Security Implementations
1. SQL Injection Prevention
We are using Prepared Statements on all html inputs of the secure login system to make sure that our system is completely secure against SQL Injection Attacks. 
2. Command Injection Prevention

## Sprint 0: Researching new SQL Security Features to Implement
1. Researched new SQL Security Features
2. Copy Lab 5 code content to our current repository
3. Modify current code to make it our own for this project
4. Implement 2 new SQL security features for Login

# Done: 
1. Researched new SQL Security Features
2. Copy Lab 5 code to our current repository
3. Implement Strong Password Policy 

## Sprint 1: Creating new DB and implementing Strong Password without bugs
1. Created new DB 
2. Implemented Strong password correctly
3. Created new Registration Page

### Sprint 1 accomplishments: 
1. Created new DB by the name of ss_team9
We created new users and granted full access to the database, therefore we had a way to connect solely into the database of our project instead of seeing all of the databases in the entire system. 
2. Implemented Strong Password Policy
Every new user that registers into our secure login system, will have to create a password that matches with our policy: at least 1 CAPITAL letter, 1 symbol, 1 number and at least 8 characters. 
3. Created new Registration page for our website so that new users could also register into the site. 

## Sprint 2: Implementing SQL Injection Protection
1. Implemented SQL Injection Protection with Prepared Statements
2. Added newUser so that after a user registers, the username and password are directly stored in the DB. 

### Sprint 2 Accomplishments: 
1. Implemented SQL Injection Protection
We implemented Prepared Statements on all user inputs available in our login system to make sure that we are preventing SQL Injections at all layers of the system. 

## Sprint 3: Implementing reCaptcha security feature
1. Researched how to implement reCaptcha
2. Started implementing reCaptcha

### Sprint 3 Accomplishments: 
1. Figured out a manual way to implement reCaptcha
We will try and implement reCaptcha manually to see if we are able to get it fully working. 
2. Figured out a way to implement reCaptcha with google api. 

## Sprint 4: Researching more SQL Security Features for our Login. 
1. RBAC 

### Sprint 4 Accomplishments: 
1. Implemented Role Based Access Control
We are differentiating between regular users and Admin Users into our website. Admin users, will be able to see all users with accounts in our website. Regular users will just be able to register into the website and login with their user information. We implemented this by adding an attribute to the Users table in our database, therefore our database now stores a users: username, password, and adminuser value. Regular users will default to a adminuser value of 0, and adminusers will have a adminuser value of 1. 

## Sprint 5: 
1. Fix DB connection issue
2. Fix reCaptcha issue

### Sprint 5 Accomplishments: 
1. Fixed DB connection issue
Fixed DB connection issue by creating a new user with access granted to the entire DB for our project. This way we could access the DB directly through that user. To fix the issue, I also had to change the credentials for the DB in the database.php file. 
2. Made progress with reCaptcha issue
reCaptcha is displaying the image properly in our login system where the text should be, but the random letters aren't displaying properly. We will try to fix this issue before our final presentation but if not, we will try to implement googles reCaptcha method. 

## Sprint 6: 
1. Fix registration issue
2. Add a page where only admin user can view all the users of the website. 
3. Fix reCaptcha issue or implement google reCaptcha


### Contribution guidelines ###
Andre
1. Created new DB on MySQL also using database.sql
2. Created a new registration page for new users. 
3. Implemented Strong Password Policy for new users creating an account. 
4. Implemented SQL Injection protection by using Prepared statements on all inputs of the login page. 
5. Documentation on README
6. Implemented Role Based Access Control
7. Continued developing reCaptcha security
8. Implemented Sessions into website
9. Fixed DB connection issue 

Anthony
1. Created new documents for reCaptcha security feature
2. Implementing reCaptcha feature to work on login page
3. Documentation on README
4. More Research on SQL Security Features we could implement
5. Started creating Final Project Report
RBAC
Verbose Errors
Isolating DB.sql from the server so attacker cannot access. 



