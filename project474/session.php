<?php
	$lifetime = 20 * 60;
	$path = "/project474";
	$domain = "192.168.56.101" ;
	$secure = TRUE;
	$httponly = TRUE;
	session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
	session_start();

	if($_SESSION["logged"] != TRUE) {
		echo "You have not logged in. Please login first";
		session_destroy();
		header("Refresh:0; url=login.php");
		die();
	}
?>