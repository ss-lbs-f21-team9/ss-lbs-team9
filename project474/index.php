<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>SS-LBS Final Project</title>
</head>
<body>
	<h1>CPS 474 Final Project:</h1>
	<h2>Secure Login Page</h2> 
   	<h2>Secure Login by <font color="blue">Andre Cullen and Anthony Avila</font>for the Final Project in CPS 474</h2>
   	
<?php 
	session_start();
	$mysqli = mysqli_connect('localhost', 'andrec', 'admin','ss_team9');
	
	if (mysqli_connect_errno()) {
    	printf("Connect failed: %s\n", mysqli_connect_error());
    	exit();
	}

	$welcome = "Welcome back "; //default message for return users
	$username = $_POST["username"]; //username input from the user via HTTP Request POST
	$password = $_POST["password"]; //password input from the user via HTTP Request POST
	//Strong Password Policy 
	/*
	$uppercase = preg_match('@[A-Z]@', $password);
	$lowercase = preg_match('@[a-z]@', $password);
	$number    = preg_match('@[0-9]@', $password);

	if(!$uppercase || !$lowercase || !$number || strlen($password) < 8) {
  		// Strong Password Policy
  		echo "Password must be at least 8 characters long and must have at least 1 number, symbol and uppercase";
	}
	*/
  	/*for debug only*/echo "DEBUG>Received: username=\"" . $username .  "\" and password=\"" . $password . "\"<br>\n";
	if (isset($username) and isset($password) ){
	//the case username and password is provided
    	if (securechecklogin($username,$password)){ 
      		$_SESSION["logged"]=TRUE;
			$_SESSION["username"] = $username;
			//$_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
			$welcome = "Welcome "; //not previously logged-in 
    	}else{//failed
			redirect_login('Invalid username/password');
		}
	}else{
		if ($_SESSION["logged"]!=TRUE) {
    		redirect_login('You have not logged in. Please login first!');
  		}
  		
	}
	//the main business logic implementation of the page
	echo "Current time: " . date("Y-m-d h:i:sa") . "\n";
	echo "<h2>" .  $welcome . "<font color='blue'>" . $_SESSION["username"] . "</font>!</h2>\n";
?>
	<a href="logout.php">Logout</a> <a href="allusers.php">All Users</a>
<?php	
	//supporting functions	
	function redirect_login($message){
		echo "<script>alert('" . $message . "');</script>\n";
		session_destroy();//clear all session variables 
		header("Refresh:0; url=login.php");
    	die();
	}
	function mockchecklogin($username, $password) {
		
		global $mysqli;
		$prepared_sql = "SELECT * FROM users WHERE username = ? " . " AND password=password(?);";

		if (($username== "cullena1@udayton.edu") and ($password == "admin")) 
		  return TRUE;
		return FALSE;	
  	}
  	function securechecklogin($username, $password) {
		//access the real database to check the username/password
		global $mysqli;
		//SQL Injection Protection
		$prepared_sql = "SELECT * FROM users WHERE username = ? " . " AND password=password(?);";
		if(!$stmt = $mysqli->prepare($prepared_sql))
			echo "Error, SQL Injection Detected";
		$stmt->bind_param("ss",$username, $password);
		if(!$stmt->execute())
			echo "Execute Error";
		$result = $stmt;
		/*
  		$sql = "SELECT * FROM users where username='" . $username . "' AND password('" . $password . "')";
  		echo "DEBUG>sql=" . $sql . "\n<br>";
  		*/

  		//$result = mysqli_query($dbconnection,$sql);
  		
  		/*if($result){
  			//check
  			$row = mysqli_fetch_assoc($result);
  			if($row['username'] === $username)
  				return TRUE;
  		}*/
  		if($result->num_rows >=1) 
  			return TRUE;
		return FALSE;
  	}
?>
</body>
</html>

