<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>SS-LBS Final Project</title>
</head>
<body>
	<h1>CPS 474 Final Project:</h1>
	<h2>Secure Login Page</h2> 
   	<h2>Secure Login by <font color="blue">Andre Cullen and Anthony Avila</font>for the Final Project in CPS 474</h2>
<?php 
	session_start();
	session_destroy();
	echo "Current time: " . date("Y-m-d h:i:sa") . "<br>\n";
?>
	<p>You are logged out. Please click <a href="login.php">here</a> to login again.</p>
</body>
</html>

