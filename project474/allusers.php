<a href="index.php">Back to HomePage</a>
<?php 
	require "session.php";
	$mysqli = new mysqli('localhost', 'andrec', 'admin','ss_team9');
	if($mysqli->connect_errno) {
		printf("Database couldn't connect %s\n", $mysqli->connect_error);
		exit();
	}
	$username = $_SESSION["username"];
	$adminuser = 1;

	$prepared_sql = "SELECT * FROM users WHERE username = ? AND adminuser = ?";
	if(!$stmt = $mysqli->prepare($prepared_sql))
		echo "Prepared Statement Error";
	$stmt->bind_param("si", $username, $adminuser);
	if(!$stmt->execute())
		echo "Error";
	$result = $stmt; 
	if(!($result->num_rows == 1)) {
		echo "Non Admin User has attempted to view users! Access Denied";
		return FALSE;
	}

	$prepared_sql = "SELECT username FROM users;";
	if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
	if(!$stmt->execute()) return FALSE; 
	$username = NULL;
	if(!$stmt->bind_result($username)) echo "Bind Fail";
	echo "All Users: ";
	while($stmt->fetch()) {
		echo htmlentities($username) . " ";
	}

?>