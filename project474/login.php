<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>SS-LBS Final Project</title>
	</head>
	<body>
		<h1>CPS 474 Final Project:</h1>
		<h2>Secure Login Page</h2> 
   		<h2>Secure Login by <font color="blue">Andre Cullen and Anthony Avila</font>for the Final Project in CPS 474</h2>
	<?php
		session_start();
		if (isset($_SESSION["logged"]) and $_SESSION["logged"] === TRUE) {
			echo "<script>alert('You have been logged in. Welcome back!');</script>";
			header("Refresh:0; url=index.php");
			exit();
	  	}
		echo "Current time: " . date("Y-m-d h:i:sa") . "<br>\n";
	?>
	    <form action="index.php" method="POST" action="verify.php">
	    
	    	Username:<input type="text" name="username" /> <br/>
			Password: <input type="password" name="password" /> <br/>

			CAPTCHA
			<div class="elem-group">
    			<label for="captcha">Please Enter the Captcha Text</label>
    			<img src="captcha.php" alt="CAPTCHA" class="captcha-image"><i class="fas fa-redo refresh-captcha"></i>
    			<br>
    			<input type="text" id="captcha" name="captcha_challenge" pattern="[A-Z]{6}">
			</div>
			
			<button type="submit">Login</button>
		</form>
		<a href="register.php">Register</a>
	</body>
</html>