DROP TABLE IF EXISTS `users`;


CREATE TABLE users(
        username varchar(50) PRIMARY KEY,
        password varchar(100) NOT NULL,
        adminuser int DEFAULT 0);

LOCK TABLES `users` WRITE; 
INSERT INTO `users` VALUES ('admin',password('Testing1!'), 1);
INSERT INTO `users` VALUES ('cullena1',password('Testing1!'), 1);
INSERT INTO `users` VALUES ('avilaa1',password('Testing1!'), 1);
INSERT INTO `users` VALUES ('pphung',password('Testing1!'), 1);
UNLOCK TABLES; 