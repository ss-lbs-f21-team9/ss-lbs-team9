<?php
    require "database.php";
    $username = $_POST['username'];
    $password = $_POST["password"];
    if (isset($username) and isset($password)) {
        if(addnewuser($username, $password)) {
            echo "<h4>You have registered successfully</h4>";
        } else {
            echo "<h4>Error: User not able to register.</h4>";
        }
    } else {
        echo "No provided username/password to change.";
        exit();
    }
?>

<a href="login.php">Login</a>